const slackAppPort = parseInt(process.env.SLACK_APP_PORT) || 8080;

app.listen(slackAppPort, "0.0.0.0", () => {
    console.log(`Slack app listening on port ${slackAppPort}.`);
});
